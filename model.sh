# tleite@bsd.com.br

echo "
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'
        alias ls='ls  --color=auto'
        alias ll='ls  --color=auto -l'
        alias l='ls  --color=auto -lA'
        alias vi='vim'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

export PS1='\[\033[01;31m\][\[\033[01;37m\]\t\[\033[01;31m\]] \[\033[01;32m\]\u\[\033[01;31m\]@\[\033[01;32m\]\h \[\033[01;31m\][\[\033[01;33m\]\w\[\033[01;31m\]] \[\033[01;37m\]→ \[\033[00m\]'

export HISTTIMEFORMAT="%d/%m/%y %T: →   "

"  >> ~/.bashrc && source ~/.bashrc